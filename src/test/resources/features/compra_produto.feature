Feature: Compra produto loja de vinho

  Eu como usuário, desejo realizar a compra de um vinho para que possa consumir o mesmo


  Scenario Outline: Adicionar produto ao carrinho
    Given que o usuário esta logado na página da divvino <userName> <password>
    And consultar um produto <produto>
    And selecionar a quantidade
    When clicar em adicionar ao carrinho
    And clicar no botão meu carrinho
    Then o usuário é levado para página para carrinho
    And produto <produto> selecionado com a quantidade <quantidade> informada

    Examples:
      | produto                           | quantidade | userName                 | password |
      | "Quinta Santa Eufêmia Porto Ruby" | 2          | "test@automation.com.br" | "123456" |