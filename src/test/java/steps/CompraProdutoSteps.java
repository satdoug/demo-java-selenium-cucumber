package steps;

import io.cucumber.java.After;
import io.cucumber.java.Before;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import pages.BuscaPage;
import pages.CarrinhoPage;
import pages.HomePage;
import pages.LoginPage;

import java.util.concurrent.TimeUnit;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;

public class CompraProdutoSteps {
    private static WebDriver driver;
    private HomePage homePage = new HomePage(driver);
    private LoginPage loginPage;
    private BuscaPage buscaPage;
    private CarrinhoPage carrinhoPage;
    private String valorProduto_esperado = "R$ 54,90";
    private String quantidadeProduto_esperado = "2";


    @Before
    public static void inicializar() {
        System.setProperty("webdriver.chrome.driver",
                "src\\drivers\\chrome\\83\\chromedriver.exe");
        driver = new ChromeDriver();
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
    }

    @Given("que o usuário esta logado na página da divvino {string} {string}")
    public void que_o_usuário_esta_logado_na_página_da_divvino(String userName, String password) {
        homePage.carregarPaginaInicial();
        loginPage = homePage.clicarBotaoEntrar();
        homePage = loginPage.logarSistema(userName, password);
    }

    @Given("consultar um produto {string}")
    public void consultar_um_produto(String produto) {
        homePage.carrinhoEstaVazio();
        homePage.PreencherCampoPesquisa(produto);
        buscaPage = homePage.clicarBotaoPesquisar();
    }

    @Given("selecionar a quantidade")
    public void selecionar_a_quantidade() {
        buscaPage.preencherQuantidade("2");
    }

    @When("clicar em adicionar ao carrinho")
    public void clicar_em_adicionar_ao_carrinho() {
        buscaPage.clicarBotaoAdicionarCarrinho();
    }

    @When("clicar no botão meu carrinho")
    public void clicar_no_botão_meu_carrinho() {
        carrinhoPage = buscaPage.clicarBotaoCarrinho();
    }

    @Then("o usuário é levado para página para carrinho")
    public void o_usuário_é_levado_para_página_para_carrinho() {
        String url_Atual = carrinhoPage.obter_urlPaginaCarrinho();
        assertThat(url_Atual, is("https://www.divvino.com.br/liquor/carrinho"));
        assertThat(carrinhoPage.obter_valorProduto(), is(valorProduto_esperado));
        assertThat(carrinhoPage.obter_quantidadeProduto(), is(quantidadeProduto_esperado));
    }

    @Then("produto {string} selecionado com a quantidade {int} informada")
    public void produto_selecionado_com_a_quantidade_informada(String produto, Integer quantidade) {

    }

    @After(order = 0)
    public static void finalizar() {
        driver.quit();
    }

}
