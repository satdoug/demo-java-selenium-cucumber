package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class LoginPage {

    private WebDriver driver;

    public LoginPage(WebDriver driver) {
        this.driver = driver;
    }

    private By inputUser = By.id("user");
    private By inputPassword = By.cssSelector(".passUser");
    private By botaoEntrar = By.id("loginSubmit");

    public void preencherUser(String texto) {
        driver.findElement(inputUser).sendKeys(texto);
    }

    public void preencherPassword(String texto) {
        driver.findElement(inputPassword).sendKeys(texto);
    }

    public void clicarBotaoEntrar() {
        driver.findElement(botaoEntrar).click();
    }

    public HomePage logarSistema(String userName, String password) {
        this.preencherUser(userName);
        this.preencherPassword(password);
        this.clicarBotaoEntrar();
        return new HomePage(driver);
    }

}
