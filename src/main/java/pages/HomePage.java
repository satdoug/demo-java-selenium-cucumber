package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class HomePage {

    private WebDriver driver;

    public HomePage(WebDriver driver) {
        this.driver = driver;
    }

    private By botaoEntrar = By.id("loginAutoUser");
    private By inputPesquisa = By.id("search_box_desktop");
    private By botaoPesquisar = By.className("icon-lupa");
    private By labelQuantidadeCarrinho = By.id("cartNumProducts");
    private By botaoCarrinho = By.cssSelector("div.cart_wrapper > a");
    private CarrinhoPage carrinhoPage;


    public void carregarPaginaInicial() {
        driver.manage().window().maximize();
        driver.get("https://www.divvino.com.br");
    }

    public LoginPage clicarBotaoEntrar() {
        driver.findElement(botaoEntrar).click();
        return new LoginPage(driver);
    }

    public void PreencherCampoPesquisa(String produto) {
        driver.findElement(inputPesquisa).sendKeys(produto);
    }

    public BuscaPage clicarBotaoPesquisar() {
        driver.findElement(botaoPesquisar).click();
        return new BuscaPage(driver);
    }

    public String obter_quantidadeCarrinho(){
        return driver.findElement(labelQuantidadeCarrinho).getText();
    }

    public CarrinhoPage clicarBotaoCarrinho(){
        driver.findElement(botaoCarrinho).click();
        return new CarrinhoPage(driver);
    }

    public void carrinhoEstaVazio() {
        int quantidadeCarrinho = Integer.parseInt(obter_quantidadeCarrinho());
        if(quantidadeCarrinho > 0){
            carrinhoPage = clicarBotaoCarrinho();
            carrinhoPage.clicarBotaoRemoverItem();
            carrinhoPage.clicarBotaoHome();
        }
    }
}
