package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;


public class CarrinhoPage {

    private WebDriver driver;

    public CarrinhoPage(WebDriver driver) {
        this.driver = driver;
    }

    private By labelValorProduto = By.xpath("html/body/div[6]/div[2]/div/div[2]/div[3]/div[2]");
    private By comboQuantidadeProduto = By.cssSelector(".carrinho_produto .division .select2-hidden-accessible");
    private By botaoRemoverItem = By.xpath("/html/body/div[6]/div[2]/div/div[2]/div[3]/div[3]/div/a");
    private By botaoHomeLogo = By.className("logo");

    public String obter_urlPaginaCarrinho() {
        return driver.getCurrentUrl();
    }

    public String obter_valorProduto() {
        return driver.findElement(labelValorProduto).getText();
    }

    public String obter_quantidadeProduto(){
        Select dropdown = new Select(driver.findElement(comboQuantidadeProduto));
        WebElement option = dropdown.getFirstSelectedOption();
        return option.getAttribute("value");
    }

    public void clicarBotaoRemoverItem(){
        driver.findElement(botaoRemoverItem).click();
    }

    public HomePage clicarBotaoHome(){
        driver.findElement(botaoHomeLogo).click();
        return new HomePage(driver);
    }
}
