package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;

public class BuscaPage {

    private WebDriver driver;

    public BuscaPage(WebDriver driver) {
        this.driver = driver;
    }

    private By inputQuantidade = By.id("quant-1746090");
    private By adicionarCarrinho = By.cssSelector(".container_btn_add > a");
    private By botaoCarrinho = By.cssSelector("#quantidadeAjax .js_cart_btn");

    public void preencherQuantidade(String quantidade) {
        WebElement quantidadeProduto = driver.findElement(inputQuantidade);
        Select select = new Select(quantidadeProduto);
        select.selectByVisibleText(quantidade);
    }

    public CarrinhoPage clicarBotaoAdicionarCarrinho() {
        driver.findElement(adicionarCarrinho).click();
        return new CarrinhoPage(driver);
    }

    public CarrinhoPage clicarBotaoCarrinho() {
        driver.findElement(botaoCarrinho).click();
        return new CarrinhoPage(driver);
    }
}
